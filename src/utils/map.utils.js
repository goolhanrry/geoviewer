/* Written by Ye Liu */

const mapStyles = {
    Streets: 'mapbox://styles/goolhanrry/cjw8xh7e200781cnprhz1wdyv',
    Outdoors: 'mapbox://styles/goolhanrry/cjw8xvtw001271do5wh8jcisr',
    Light: 'mapbox://styles/goolhanrry/cjw8xfxnz00ov1ctbo72e8tvq',
    Dark: 'mapbox://styles/goolhanrry/cjw8x7qcb036z1cpakzz43vsv',
    Night: 'mapbox://styles/goolhanrry/cjw8xt95o05f01cpc101wyn5s',
    LeShine: 'mapbox://styles/goolhanrry/cjw8xn3y562m31cmngt2pzg2z',
    NorthStar: 'mapbox://styles/goolhanrry/cjw8xy5el01441do5lkxunsjn',
    Moonlight: 'mapbox://styles/goolhanrry/cjw8yqo2q01v11cqkk9e6iviq'
};

export { mapStyles };
